# Shmup Projet
Un Shmup pour apprendre quelques bases avec Unity : 
- Usage des Sprites
- Collisions / Physique (RigibBody, Collider, Physics settings (collision matrix)).
- Premier Scripts C#

Enjeu : 
Produire du GAMEPLAY!!!  
(et pour cela se casser les dents sur la tâche ô combien passionante mais sévère, âpre, austère du développement informatique).

[Ici, les scripts à utiliser comme base de jeu](https://gitlab.com/jniac-unity/shmup-project/-/raw/master/AssetsScripts.zip)


<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
## Memo
Le zip de script est fait avec [glob-zip](https://www.npmjs.com/package/glob-zip) : 
```shell
glob-zip Scripts.zip Assets/Scripts/**/*.cs
# with git
glob-zip Scripts.zip Assets/Scripts/**/*.cs && ga Scripts.zip && gc -m "zip update"
```
